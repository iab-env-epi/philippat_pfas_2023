---
title: "Associations between PFAS and child respiratory health"
subtitle: "Sensitivity analysis - not show in the article"
output: 
  prettydoc::html_pretty:
    theme: tactile
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Clustering

After clustering, we need to check some statistical criteria including stability, distribution within classes, posterior probability of inclusions and discriminative power.  
The discriminative power evaluates the importance of the exposure in the clustering. A negative discriminative power means that the exposure does not influence clustering and a discriminative power closes to 0 means a weak influence in the clustering. Conversely, the higher the discriminative power, the more significant the exposure is in the clustering.

## With all included exposures (N = 13)

First, clustering was performed with all PFAS included (continuous and categorical exposures). But discriminative power suggested that the majority of categorical PFAS had low or no influence. 

```{r, echo = F}
knitr::include_graphics("../Figs/Fig_data_not_show_discriminative.pdf")
```

`*` Represents categorcial exposures coding with 2 categories: 1) not quantified and 2) quantified. The first category is the reference.  

## With continuous exposures (N = 7), final version

Given the above results, we tested clustering without the categorical PFAS.  
Our results were similar with fewer exposures (better understanding and interpretation of classes) and all PFASs influenced in clustering.

```{r, echo=FALSE}
knitr::include_graphics("../Figs/Fig_data_not_show_discriminative_power.pdf")
```

