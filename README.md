---
title: "In utero exposure to poly- and perfluoroalkyl substances an dchildren respiratory health up to 36 months in a French mother-child cohort"
---

# R

All the scripts needed to produce results presented in the manuscript analyses are available in this folder. They should be run in the order presented below :

**functions.R**: Custom functions needed in the analysis. It contains a function that returns a description of the variable (mean, sd, N for continuous variable and N (%) per category for categorical variable) and another that returns a table with the results of the regressions for each given outcome and exposure (beta/OR, CI, pvalue). The regressions can be adjusted or unadjusted, linear or logistic.

**0_Variable_fish_consumption.R**: coding of the fish consumption variable adjustment factor 

**1_Creation_bdd.R**: selection of the study population – coding of the covariate (but fish consumption) – multiple imputation of the co-variates 

**2_Analyses_descriptives.R**: Descriptive analyses

**3_ExWAS.R**: Uni-pollutant analysis (associations between each PFAS and each outcome) 

**4_ExWAS_sensitivity_analyses_060922.R**: Sensitivity analyses for the uni-pollutants model (non-standardized exposure concentrations, with fish consumption adjustment factor, without outliers, interaction with child gender). Results are presented in the Supplemental Material

**5_clusters.R**: Clustering of exposure variables, cluster generation and associations between clusters and respiratory outcomes. Also contains P-trend for the cluster analysis.

**6_BKMR.R**: Handles BKMR models generation and graphic generation

# Figs

All figures included in the manuscript and its Supplemental Material can be found in this folder.

# Tabs

All tables included in the manuscript and its Supplemental Material can be found in this folder.

# Results

Document presenting the results of the manuscript (tables and figures): `main_results.Rmd`  
Document presenting the results of the Supplemental Material (tables and figures): `Supplemental_Material.Rmd`  
Document presenting the results which are not included in the manuscript (indicated in the manuscript as 'data not show'): `Sensitivity_analyses.Rmd` 
