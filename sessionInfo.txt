R version 4.0.5 (2021-03-31)
Platform: x86_64-apple-darwin17.0 (64-bit)
Running under: macOS Big Sur 10.16

Matrix products: default
LAPACK: /Library/Frameworks/R.framework/Versions/4.0/Resources/lib/libRlapack.dylib

locale:
[1] fr_FR.UTF-8/fr_FR.UTF-8/fr_FR.UTF-8/C/fr_FR.UTF-8/fr_FR.UTF-8

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] forcats_0.5.1   stringr_1.4.0   purrr_0.3.4     readr_1.4.0     tidyr_1.1.3    
 [6] tibble_3.1.1    ggplot2_3.3.3   tidyverse_1.3.1 dplyr_1.0.5     haven_2.4.0    

loaded via a namespace (and not attached):
 [1] httr_1.4.2          jsonlite_1.7.2      splines_4.0.5       prettydoc_0.4.1    
 [5] modelr_0.1.8        Formula_1.2-4       shiny_1.6.0         assertthat_0.2.1   
 [9] highr_0.9           latticeExtra_0.6-29 cellranger_1.1.0    yaml_2.2.1         
[13] pillar_1.6.0        backports_1.2.1     lattice_0.20-41     glue_1.4.2         
[17] digest_0.6.27       RColorBrewer_1.1-2  promises_1.2.0.1    checkmate_2.0.0    
[21] rvest_1.0.0         colorspace_2.0-0    cowplot_1.1.1       htmltools_0.5.1.1  
[25] httpuv_1.6.2        Matrix_1.3-2        pkgconfig_2.0.3     broom_0.7.9        
[29] labelled_2.8.0      questionr_0.7.4     xtable_1.8-4        scales_1.1.1       
[33] jpeg_0.1-8.1        later_1.1.0.1       htmlTable_2.1.0     generics_0.1.0     
[37] ellipsis_0.3.1      withr_2.4.2         nnet_7.3-15         cli_3.6.0          
[41] readxl_1.3.1        survival_3.2-10     magrittr_2.0.1      crayon_1.4.1       
[45] mime_0.10           evaluate_0.14       fs_1.5.0            fansi_0.4.2        
[49] xml2_1.3.2          foreign_0.8-81      tools_4.0.5         data.table_1.14.0  
[53] hms_1.0.0           lifecycle_1.0.0     reprex_2.0.0        munsell_0.5.0      
[57] cluster_2.1.2       compiler_4.0.5      tinytex_0.31        rlang_0.4.10       
[61] grid_4.0.5          rstudioapi_0.13     htmlwidgets_1.5.3   miniUI_0.1.1.1     
[65] base64enc_0.1-3     rmarkdown_2.7       gtable_0.3.0        DBI_1.1.1          
[69] R6_2.5.0            lubridate_1.7.10    gridExtra_2.3       knitr_1.32         
[73] fastmap_1.1.0       utf8_1.2.1          Hmisc_4.5-0         stringi_1.5.3      
[77] Rcpp_1.0.6          vctrs_0.3.7         rpart_4.1-15        png_0.1-7          
[81] dbplyr_2.1.1        tidyselect_1.1.0    xfun_0.22          
