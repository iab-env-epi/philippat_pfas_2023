#Creation de la variable consommation de poisson à T1
#2022/10/18
#Ophélie

library(haven)
library(dplyr)

bdd <- read_sas('../0_Base_initiale/data_ophelie_221014.sas7bdat')
bdd_poisson_T1 <- bdd %>%
  select(ident, mt1ean1_q51:mt1ean1_q57, mt2ean2_q51, mt3ean3_q51) 

#Etape 1 : variable poisson en général
bdd_poisson_T1 <- bdd_poisson_T1 %>%
  mutate(poisson_T1 = mt1ean1_q51) 

#Etape 2 :supprimer des NA de la variable poisson_T1
#Si conso de poisson spécifique (q52 à q55) au cours du T1 alors prendre le
# max des fréquences de consommation de tous les poissons spécifiques
bdd_poisson_T1 <- bdd_poisson_T1 %>%
  mutate_at(vars(poisson_T1), ~ ifelse(is.na(.x), pmax(mt1ean1_q52, mt1ean1_q53,
                                                       mt1ean1_q54, mt1ean1_q55), .x))

#Etape 3 : supprimer des NA de la variable poisson_T1
#Si conso de plats à base de poisson au cours du T1 alors 
# poisson_T1 = fréquence de consommation de plats à base de poisson
bdd_poisson_T1 <- bdd_poisson_T1 %>%
  mutate_at(vars(poisson_T1), ~ ifelse(is.na(.x), mt1ean1_q57, .x))

#Etape 4 : supprimer des NA de la variable poisson_T1
#Si conso de poisson pané au cours du T1 alors
# poisson_T1 = fréquence de consommation de poisson pané
bdd_poisson_T1 <- bdd_poisson_T1 %>%
  mutate_at(vars(poisson_T1), ~ ifelse(is.na(.x), mt1ean1_q56, .x))

#Etape 5 : supprimer des NA de la variable poisson_T1
#Si conso de poisson à T2 ou T3 alors 
# poisson_T1 = max(fréquence de conso de poisson à T2 et T3)
bdd_poisson_T1 <- bdd_poisson_T1 %>%
  mutate_at(vars(poisson_T1), ~ ifelse(is.na(.x),
                                       pmax(mt2ean2_q51, mt3ean3_q51, na.rm = T), .x))

#Recodage de la variable finale : 2 catégories
#Cat 0 = conso faible ou nulle -> Cat 1, 2, 3
#Cat 1 = conso réguilère ou élevée -> Cat 4, 5, 6, 7
bdd_poisson_T1 <- bdd_poisson_T1 %>%
  mutate(fish_consumption_T1 = case_when(!is.na(poisson_T1) & 
                                            (poisson_T1 == 1 | poisson_T1 == 2 |poisson_T1 == 3) ~ '0',
                                          !is.na(poisson_T1) &
                                            (poisson_T1 == 4 | poisson_T1 == 5 | poisson_T1 == 6 | poisson_T1 == 7 ) ~ '1',
                                          is.na(poisson_T1) ~ 'NA')) %>%
  mutate_at(vars(fish_consumption_T1), as.factor)


#Sauvegarde de la base de données poisson à T1
write.table(bdd_poisson_T1, '../1_Base_intermediaire/bdd_poisson_T1.csv',
            sep=";", row.names = F)
