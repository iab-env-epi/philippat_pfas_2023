library(bkmr)
library(future)
library(dplyr)
library(tidyr)
library(stringr)
library(ggplot2)

plan(multisession)

db = data.table::fread("../1_Base_intermediaire/bdd_N450PFAS_imputee_nettoyee_221216.csv")

### Creating list to hold the relevant data based on the time period

outcome_covariate_list = list(
    list_3y_FOT = list(
        outcomes = c("X7_mean", "R7_mean", "AX_mean", "R7_19_mean"),
        covariates = c("ch_sex", "ch_tob_expo_3y", "dipl", "ch_still_bf_m2", "season_1", "season_2", "season_3", "parity", 
            "height_cm", "mo_bmi_bepr", "mo_age", "weight", "ATCD_rhinite")
    ),
    list_2_month = list(
        outcomes = c("ch2m_LCI2_corrHV", "ch2m_CRF2_corrHV", "tidalvolumeml", "ratiopefexptime"),
        covariates = c("ch_sex", "ch_tob_expo_2m", "dipl", "ch_still_bf_m2", "season_1", "season_2", "season_3", 
            "parity", "taille_2m", "mo_bmi_bepr", "mo_age", "ATCD_rhinite", "poids_2m")
    ),
    list_3y_quest = list(
        outcomes = c("ch_asthma_0_3y_opt", "ch_wheez_0_3y_opt", "ch_bronchitis_0_3y_opt", "ch_bronchiolitis_0_2y_opt"),
        covariates = c("ch_sex", "ch_tob_expo_3y", "dipl", "ch_still_bf_m2", "season_1", "season_2", "season_3", 
            "parity", "mo_bmi_bepr", "mo_age", "ATCD_rhinite", "weight")
    )
)

### Slight corrections

#We need to fix the classes of the outcomes, because the .csv treats everything as a string/numerical

db = db |>
    mutate(across(all_of(c(c("ch_asthma_0_3y_opt", "ch_wheez_0_3y_opt", "ch_bronchitis_0_3y_opt", "ch_bronchiolitis_0_2y_opt"))), as.factor))

#The 'season' variable needs to be broken down in binary variables

season_dummies <- model.matrix(~ factor(season), data = db)
colnames(season_dummies) <- paste("season", levels(factor(db$season)), sep = "_")
season_dummies <- season_dummies[, -1]

db = cbind(db, season_dummies)

### PFAS vectors

num_PFAS_vec = c("mo_PFOA_cor", "mo_PFNA", "mo_pfda_i_cor", "mo_pfunda_i_cor",
              "mo_PFHxS_cor", "mo_pfhps_i_cor", "mo_PFOS_cor")

### Run

#We are going to take the 10th imputated dataset to run the models.

imp_to_use = 10

dataset = db |>
    filter(.imp == imp_to_use)

#Run loop

result_list = list()

if (!dir.exists(paste0("../3_Resultats/5_BKMR/Files/", imp_to_use, "/"))) {
    dir.create(paste0("../3_Resultats/5_BKMR/Files/", imp_to_use, "/"), showWarnings = FALSE)
}

if (!dir.exists(paste0("../3_Resultats/5_BKMR/Figures/", imp_to_use, "/"))) {
    dir.create(paste0("../3_Resultats/5_BKMR/Figures/", imp_to_use, "/"), showWarnings = FALSE)
}

for (list_name in names(outcome_covariate_list)) {
    sub_list = outcome_covariate_list[[list_name]]
    for (outcome in sub_list$outcomes) {
        #For now, we are not trying to make "categorical" BKMR
        if (class(dataset[[outcome]]) == "factor") {
            next()
        }

        result_list[[outcome]] = future({
            #Creating a subdataset with only the relevant variables
            sub_dataset = dataset |>
                select(all_of(num_PFAS_vec), all_of(sub_list$covariates), all_of(outcome)) |>
                drop_na()
            
            #Splitting in different matrixes
            expo_matrix = sub_dataset |>
                select(all_of(num_PFAS_vec)) |>
                mutate(across(all_of(num_PFAS_vec), scale)) |>
                as.matrix()

            outcome_matrix = sub_dataset |>
                select(all_of(outcome)) |>
                as.matrix()

            cov_matrix = sub_dataset |>
                select(all_of(sub_list$covariates)) |>
                as.matrix()

            bkmr_model = kmbayes(outcome_matrix, expo_matrix, cov_matrix, iter=50000)

            saveRDS(bkmr_model, file=paste0("../3_Resultats/5_BKMR/Files/", imp_to_use, "/", outcome, "_model.rds"))

            risks_overall <- OverallRiskSummaries(fit = bkmr_model, y = outcome_matrix, Z = expo_matrix, X = cov_matrix, 
                                      qs = seq(0.1, 0.75, by = 0.05), 
                                      q.fixed = 0.1, method = "exact")
            
            saveRDS(risks_overall, file=paste0("../3_Resultats/5_BKMR/Files/", imp_to_use, "/", outcome, "_overall_risk.rds"))

            risks_singvar <- SingVarRiskSummaries(fit = bkmr_model, y = outcome_matrix, Z = expo_matrix, X = cov_matrix, 
                                      qs.diff = c(0.25, 0.75), 
                                      q.fixed = c(0.25, 0.50, 0.75),
                                      method = "exact")
            
            saveRDS(risks_singvar, file=paste0("../3_Resultats/5_BKMR/Files/", imp_to_use, "/", outcome, "_singvar_risk.rds"))

            return(outcome)
        }, seed = 111)
    }
}

result_list = lapply(result_list, value)

### Generating graphs 

for (file in list.files(path=paste0("../3_Resultats/5_BKMR/Files/", imp_to_use, "/"), full.names = TRUE)) {
    if (str_detect(file, "model")) {
        next()
    }

    data = readRDS(file)

    filename = str_split(file, "/")[[1]]
    filename = filename[[length(filename)]]
    filename = str_extract(filename, ".*?(?=\\.)")
    outcome = str_replace(filename, "_singvar_risk|_overall_risk", "")
    plotname = paste0(filename, ".png")

    if (str_detect(file, "overall")) {
        plot = ggplot(data, aes(quantile, est, ymin = est - 1.96*sd, ymax = est + 1.96*sd)) + 
            geom_pointrange() +
            geom_hline(aes(yintercept = 0), col="red") + 
            labs(y = outcome)
    } else {
        plot = ggplot(data, aes(variable, est, ymin = est - 1.96*sd, ymax = est + 1.96*sd, col = q.fixed)) + 
            geom_pointrange(position = position_dodge(width = 0.75)) + 
            coord_flip()
    }

    ggsave(plot = plot, filename=plotname, path=paste0("../3_Resultats/5_BKMR/Figures/", imp_to_use, "/"))
}
