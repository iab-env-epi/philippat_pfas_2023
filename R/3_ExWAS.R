#Title: ExWAS analyses, PENDORE project - 3 years & 2 months
#Date: September, 2022
#Author: Ophelie

library(haven) #To read data base (from SAS)
library(tidyr)
library(tidyverse)
library(ggplot2) #To make graph
library(lubridate) #To work with date, time
library(zoo) #To work with date, time
library(dplyr)
library(mice) #For MI functions


bdd_initiale <- read.csv('../1_Base_intermediaire/bdd_N450PFAS_imputee_nettoyee_221216.csv')
source('functions.R')

#--------------------------------Data set---------------------------------------
bdd <- bdd_initiale %>%
  mutate_at(vars(ch_sex, ch_tob_expo_3y, ch_tob_expo_2m, dipl, ch_still_bf_m2,
                 season, parity, ATCD_asthme, ATCD_rhinite, inh_all_sens_SPT_2,
                 daycare_up1y_3c, ch_wh_bron_ast, ch_asthma_0_3y_opt, ch_wheez_0_3y_opt,
                 ch_bronchiolitis_0_2y_opt, ch_bronchitis_0_3y_opt,
                 mo_PFBS_cat, mo_PFHpA_cat, mo_PFDoDA_cat,
                mo__6_2diPAP_cat,
                 mo_PFHxPA_cat, mo_PFTrDA_cat), as.factor)
#PFOSA and 8:2diPAP are quantified in less than 5% => Exclusion

#-----------------------------Reduction of exposures concentrations-------------
var_expo <- c('mo_PFOA_cor', 'mo_PFNA','mo_pfda_i_cor', 'mo_pfunda_i_cor',
              'mo_PFHxS_cor', 'mo_pfhps_i_cor', 'mo_PFOS_cor')

#     !! Only for figures not for tables in the article !! 
#We reduce all continuous exposure to be able to compare betas
#bdd <- bdd %>%
#  mutate_at(var_expo, ~ scale(as.matrix(.x), center = FALSE,
#                              scale = apply(as.matrix(.x), 2, sd, na.rm = TRUE) %>% as.vector))

#--------------------------------AOS parameters---------------------------------
#We keep children with 3 acceptable AOS curves 
#qualite_R7 == 1 or qualite_R7 == 3
#N = 281
qualite_R7 <- read_sas("../0_Base_initiale/data_ophelie_221014.sas7bdat", NULL) %>%
  select(ident, qualite_R7)

bdd_3yFOT <- left_join(bdd, qualite_R7, by = 'ident') %>%
  filter(qualite_R7 == 1 | qualite_R7 == 3)

#--------------------------------ExWAS------------------------------------------
vars_expo <- c(var_expo,
               'mo_PFHpA_cat', 'mo_PFDoDA_cat', 'mo_PFTrDA_cat', 'mo_PFBS_cat',
               'mo__6_2diPAP_cat', 'mo_PFHxPA_cat')


name <-  c(var_expo, 
           'mo_PFHpA_>=LOQ','mo_PFDoDA_>=LOQ',
           'mo_PFTrDA_>=LOQ', 'mo_PFBS_>=LOQ', 'mo__6_2diPAP_>=LOQ',
           'mo_PFHxPA_>=LOQ')



##############################
#Adjusted linear regression
##############################
#--------3y FOT
outcome <- c('X7_mean', 'R7_mean', 'AX_mean', 'R7_19_mean')
tab_tot <- as.data.frame(name)

#------------------SM Table S9: Start-----------------------------------------------
for(outc in outcome){
  tab_reg <- regression_lineaire(vars = vars_expo, outcome = outc, data = bdd_3yFOT,
                                 name = name, adjusted = TRUE, time = '3y_FOT') %>%
    mutate(CI = paste0('[', round(IC_inf, 2), ', ', round(IC_sup, 2), ']')) %>%
    mutate_at(vars(beta, p_val), ~round(.x, 2)) %>%
    select(-c('IC_inf', 'IC_sup')) %>%
    mutate(var_outcome = paste0(outc, ' (n = 269)')) 
  
  tab_reg_interac <- regression_lineaire_interac(vars = vars_expo, outcome = outc, data = bdd_3yFOT,
                                         name = name, time = '3y_FOT') %>%
    mutate_at(vars(p_interac), ~ round(.x, 2)) %>%
    as.data.frame()
  
  tab_reg <- left_join(tab_reg, tab_reg_interac, by = 'name') %>%
    relocate('name', 'beta', 'CI', 'p_val', 'p_interac', 'var_outcome')
  
  tab_tot <- tab_tot %>% left_join(tab_reg, by = 'name')
}  
path <- paste0('../../3_Articles/Tableaux/TabS9.csv')
write_csv(tab_tot, path)
#------------------SM Table S9: End-------------------------------------------------

#--------2m
outcome <- c('ch2m_LCI2_corrHV', 'ch2m_CRF2_corrHV', 'tidalvolumeml', 'ratiopefexptime')
tab_tot <- as.data.frame(name)

#------------------SM Table S8: Start-----------------------------------------------
for(outc in outcome){
  tab_reg <- regression_lineaire(vars = vars_expo, outcome = outc, data = bdd,
                                 name = name, adjusted = TRUE, time = '2_months') %>%
    mutate(CI = paste0('[', round(IC_inf, 2), ', ', round(IC_sup, 2), ']')) %>%
    mutate_at(vars(beta, p_val), ~round(.x, 2)) %>%
    select(-c('IC_inf', 'IC_sup')) %>%
    mutate(var_outcome = ifelse(str_detect(outc, 'ch2m'), paste0(outc, ' (n = 333)'),
                                paste0(outc, ' (n = 311)')))
  
  tab_reg_interac <- regression_lineaire_interac(vars = vars_expo, outcome = outc, data = bdd,
                                                 name = name, time = '2_months') %>%
    mutate_at(vars(p_interac), ~round(.x, 2)) %>%
    as.data.frame()
  
  tab_reg <- left_join(tab_reg, tab_reg_interac, by = 'name') %>%
    relocate('name', 'beta', 'CI', 'p_val', 'p_interac', 'var_outcome')
  
  tab_tot <- tab_tot %>% left_join(tab_reg, by = 'name')
} 
path <- paste0('../../3_Articles/Tableaux/TabS8.csv')
write_csv(tab_tot, path)

#------------------SM Table S8: End-------------------------------------------------

#--------3y quest
outcome <- c('ch_asthma_0_3y_opt', 'ch_wheez_0_3y_opt', 'ch_bronchiolitis_0_2y_opt',
             'ch_bronchitis_0_3y_opt')
tab_tot <- c()

#------------------SM Table S10: Start-----------------------------------------------
for(outc in outcome){
  tab_reg <- regression_lineaire(vars = vars_expo, outcome = outc, data = bdd,
                                 name = name, adjusted = TRUE, time = '3y_quest') %>%
    mutate(CI = paste0('[', round(IC_inf, 2), ', ', round(IC_sup, 2), ']')) %>%
    mutate_at(vars(beta, p_val), ~round(.x, 2)) %>%
    select(-c('IC_inf', 'IC_sup')) %>%
    mutate(var_outcome = outc)
  
  tab_tot <- rbind(tab_tot, tab_reg)
}

#Low N per category for Asthma - PFHpA, Asthma - 6:2diPAP, Bronchiolitis - 6:2diPAP, Bronchitis - 6:2diPAP => not showed
tab_tot <- tab_tot %>% mutate_at(vars(beta, p_val, CI),
                                 ~ case_when(str_detect(tab_tot$var_outcome, 'asthma') & str_detect(tab_tot$name, 'PFHpA') |
                                             str_detect(tab_tot$var_outcome, 'asthma') & str_detect(tab_tot$name, '6_2diPAP') ~ '-',
                                           str_detect(tab_tot$name, '6_2diPAP') & (str_detect(tab_tot$var_outcome, 'bronchitis') | str_detect(tab_tot$var_outcome, 'bronchiolitis')) ~ '-',
                                           !(str_detect(tab_tot$name, '6_2diPAP') & (str_detect(tab_tot$var_outcome, 'asthma') |str_detect(tab_tot$var_outcome, 'bronchitis') | str_detect(tab_tot$var_outcome, 'bronchiolitis'))) |
                                               !(str_detect(tab_tot$name, 'PFHpA') & str_detect(tab_tot$var_outcome, 'ashmta')) ~ as.character(.x)))

tab_tot <- tab_tot %>% pivot_wider(names_from = 'var_outcome', values_from = c('beta', 'CI', 'p_val')) %>%
  relocate(name, paste0(c('beta_', 'CI_', 'p_val_'), outcome[1]),
           paste0(c('beta_', 'CI_', 'p_val_'), outcome[2]), 
           paste0(c('beta_', 'CI_', 'p_val_'), outcome[3]),
           paste0(c('beta_', 'CI_', 'p_val_'), outcome[4]))

path <- paste0('../../3_Articles/Tableaux/TabS10.csv')
write_csv(tab_tot, path)

#------------------SM Table S10: End-------------------------------------------------


#--------------------------Fig3: Start------------------------------------------
name <-  c('mo_PFOA_cor', 'mo_PFNA','mo_pfda_i_cor', 'mo_pfunda_i_cor',
           'mo_PFHxS_cor', 'mo_pfhps_i_cor', 'mo_PFOS_cor',
           'mo_PFHpA_cat', 'mo_PFDoDA_cat', 'mo_PFTrDA_cat', 'mo_PFBS_cat',
           'mo__6_2diPAP_cat', 'mo_PFHxPA_cat')

expo <- c('PFOA', 'PFNA', 'PFDA', 'PFUnDA', 'PFHxS', 'PFHpS', 'PFOS', 'PFHpA*',
          'PFDoDA*', 'PFTrDA*', 'PFBS*', '6:2diPAP*', 'PFHxPA*')

#--------3y FOT
outcome <- c('X7_mean', 'R7_mean', 'AX_mean', 'R7_19_mean')
bdd_3yFOT <- bdd_3yFOT %>%
  mutate_at(outcome, ~ scale(as.matrix(.x), center = FALSE,
                             scale = apply(as.matrix(.x), 2, sd, na.rm = TRUE) %>% as.vector))

y3 <- c()

for(outc in outcome){
  tab_reg <- regression_lineaire(vars = name, outcome = outc, data = bdd_3yFOT,
                                 name = expo, adjusted = TRUE, time = '3y_FOT') %>%
    mutate(var_outcome = outc)
  
  y3 <- rbind(y3, tab_reg)
}  

y3 <- y3 %>%
  mutate_at(vars(var_outcome, name), as.factor)

levels(y3$var_outcome) <- c("AX ~ ('N = 269')", "R[7-19] ~ ('N = 269')",
                            "R[7] ~ ('N = 269')", "X[7] ~ ('N = 269')")

levels(y3$name) <- c('6:2diPAP*','PFBS*','PFDA', 'PFDoDA*','PFHpA*','PFHpS','PFHxPA*',
                     'PFHxS','PFNA', 'PFOA', 'PFOS', 'PFTrDA*',  'PFUnDA')

y3 <- y3 %>% mutate(var_outcome = factor(var_outcome,
                                         levels = c("X[7] ~ ('N = 269')", "R[7] ~ ('N = 269')",
                                                    "AX ~ ('N = 269')", "R[7-19] ~ ('N = 269')"))) %>%
  mutate(name = factor(name, expo))

p2 <- ggplot(data = y3, aes(x = beta, y = name)) +
  geom_point(position = position_dodge(width = 0.8), size = 2) +
  ylab("")+
  geom_vline(xintercept = 0, linetype = "dotted") +
  geom_errorbar(data = y3,
                aes(xmin = IC_inf, xmax = IC_sup, y = name),
                position = position_dodge(width = 0.8)) +
  facet_grid(var_outcome ~ . , scales="free", labeller = label_parsed) +
  theme_light(base_size = 12) + xlab("effect estimates") +
  ggtitle('lung function at 36 months') + xlim(-1.1, 1.1)

#--------2m
outcome <- c('ch2m_LCI2_corrHV', 'ch2m_CRF2_corrHV', 'tidalvolumeml', 'ratiopefexptime')
bdd <- bdd %>%
  mutate_at(outcome, ~ scale(as.matrix(.x), center = FALSE,
                             scale = apply(as.matrix(.x), 2, sd, na.rm = TRUE) %>% as.vector))
m2 <- c()

for(outc in outcome){
  tab_reg <- regression_lineaire(vars = name, outcome = outc, data = bdd,
                                 name = expo, adjusted = TRUE, time = '2_months') %>%
    mutate(var_outcome = outc)
  
  m2 <- rbind(m2, tab_reg)
} 

m2 <- m2 %>%
  mutate_at(vars(var_outcome, name), as.factor)

levels(m2$var_outcome) <- c("FRC ~ ('N = 333')", "LCI ~ ('N = 333')",
                            "t[PTEF]/t[E] ~ ('N = 311')", "V[T] ~ ('N = 311')")

levels(m2$name) <- c('6:2diPAP*','PFBS*','PFDA', 'PFDoDA*','PFHpA*','PFHpS','PFHxPA*',
                     'PFHxS','PFNA', 'PFOA', 'PFOS', 'PFTrDA*',  'PFUnDA')

m2 <- m2 %>% mutate(var_outcome = factor(var_outcome,
                                         levels = c("LCI ~ ('N = 333')", "FRC ~ ('N = 333')",
                                                    "V[T] ~ ('N = 311')", "t[PTEF]/t[E] ~ ('N = 311')"))) %>%
  mutate(name = factor(name, expo))

p1 <- ggplot(data = m2, aes(x = beta, y = name)) +
  geom_point(position = position_dodge(width = 0.8), size = 2) +
  ylab("")+
  geom_vline(xintercept = 0, linetype = "dotted") +
  geom_errorbar(data = m2,
                aes(xmin = IC_inf, xmax = IC_sup, y = name),
                position = position_dodge(width = 0.8)) +
  facet_grid(var_outcome ~ . , scales="free", labeller = label_parsed) +
  theme_light(base_size = 12) + xlab("effect estimates") + xlim(-1, 1) +
  ggtitle('lung function at 2 months')

#--------3y quest
outcome <- c('ch_asthma_0_3y_opt', 'ch_wheez_0_3y_opt', 'ch_bronchitis_0_3y_opt',
             'ch_bronchiolitis_0_2y_opt')
y3q <- c()

for(outc in outcome){
  tab_reg <- regression_lineaire(vars = name, outcome = outc, data = bdd,
                                 name = expo, adjusted = TRUE, time = '3y_quest') %>%
    mutate(var_outcome = outc)
  
  y3q <- rbind(y3q, tab_reg)
}

y3q <- y3q %>%
  mutate_at(vars(var_outcome, name), as.factor)


levels(y3q$var_outcome) <- c("Asthma ~ ('N =  404')",  "Bronchiolitis ~ ('N = 403')", "Bronchitis ~ ('N = 404')",
                             "Wheeze ~ ('N = 405')")

levels(y3q$name) <- c('6:2diPAP**','PFBS*','PFDA', 'PFDoDA*','PFHpA**','PFHpS','PFHxPA*',
                      'PFHxS','PFNA', 'PFOA', 'PFOS', 'PFTrDA*',  'PFUnDA')

expo <- c('PFOA', 'PFNA', 'PFDA', 'PFUnDA', 'PFHxS', 'PFHpS', 'PFOS', 'PFHpA**',
          'PFDoDA*', 'PFTrDA*', 'PFBS*', '6:2diPAP**', 'PFHxPA*')

y3q <- y3q %>% mutate(var_outcome = factor(var_outcome,
                                           levels = c("Asthma ~ ('N =  404')", "Wheeze ~ ('N = 405')",
                                                      "Bronchiolitis ~ ('N = 403')", "Bronchitis ~ ('N = 404')"))) %>%
  mutate(name = factor(name, expo)) %>%
  mutate_at(vars(beta, IC_inf, IC_sup), ~ case_when(name == 'PFHpA**' & var_outcome == "Asthma ~ ('N =  404')" ~ 'NA',
                                                    name == '6:2diPAP**' & (var_outcome == "Asthma ~ ('N =  404')" | var_outcome == "Bronchitis ~ ('N = 404')"| var_outcome == "Bronchiolitis ~ ('N = 403')") ~ 'NA',
                                                    name != '6:2diPAP**' | name != 'PFHpA**' |
                                                      var_outcome == "Asthma ~ ('N =  404')" ~ as.character(.x))) %>%
  mutate_at(vars(beta, IC_inf, IC_sup), as.numeric)

p3 <- ggplot(data = y3q, aes(x = beta, y = name)) +
  geom_point(position = position_dodge(width = 0.8), size = 2) +
  ylab("")+
  geom_vline(xintercept = 1, linetype = "dotted") +
  geom_errorbar(data = y3q,
                aes(xmin = IC_inf, xmax = IC_sup, y = name),
                position = position_dodge(width = 0.8)) +
  facet_grid(var_outcome ~ . , scales="free", labeller = label_parsed) +
  theme_light(base_size = 12) + xlab("OR") + xlim(0, 4) +
  ggtitle('respiratory health diseases until 36 months')

p <- cowplot::plot_grid(p1, p2, p3, nrow = 1, ncol = 3)
ggsave(plot = p, '../../3_Articles/Figures/Fig3.pdf', dpi = 600, width = 15, height = 7)
#--------------------------Fig3: End--------------------------------------------