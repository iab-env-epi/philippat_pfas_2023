#Title: Description of respiratory data, PENDORE project - 3 years & 2 months - PFAS
#Date: September, 2022
#Author: Ophélie

library(haven) #To read data base (from SAS)
library(tidyr)
library(tidyverse)
library(ggplot2) #To make graph
library(lubridate) #To work with date, time
library(zoo) #To work with date, time
library(car) #To calculate VIF
library(dplyr)
library(Hmisc) #For correlations
library(corrplot) #For correlations
library(mice) #For MI

source('functions.R')

#################
#Download data 
#################
data <- read_sas("../0_Base_initiale/data_ophelie_221014.sas7bdat",
                         NULL)

bdd_PFAS <- read.csv("../0_Base_initiale/PFAS_2022_08_26.csv")
bdd_fish_consumption_T1 <- read.csv("../1_Base_intermediaire/bdd_poisson_T1.csv",
                                    sep = ";")

bdd <- reduce(list(data, bdd_PFAS, bdd_fish_consumption_T1),
              function(x, y){left_join(x, y, by = 'ident')})

#-------------------------creation of our data set------------------------------

#We suppress women who don't have exposure values (no PFAS)
bdd <- bdd %>% filter(!is.na(mo_PFTrDA_cat) |
                        !is.na(mo_PFNA))

#-------------------------respiratory health------------------------------------

####
#R7
####
R7 <- as.data.frame(bdd$R7_mean) %>%
  na.omit(bdd$R7_mean) %>%
  rename('R7_mean' = 'bdd$R7_mean')

#------------density plot------------------------------------
ggplot(data = R7, aes(R7_mean, y = ..density..)) +
  geom_histogram(binwidth = 0.5) +
  geom_density(colour = 'purple') + 
  labs(title = "R7 mean", x = "hPa.s/l")
#------------------------------------------------------------

description(select(bdd, R7_mean), 'continuous')

####
#X7
####
X7 <- as.data.frame(bdd$X7_mean) %>%
  filter(!is.na(bdd$X7_mean))  %>%

#------------density plot------------------------------------
ggplot(data = X7, aes(X7_mean, y = ..density..)) +
  geom_histogram(binwidth = 0.5) +
  geom_density(colour = 'purple') + 
  labs(title = "X7 mean", x = "hPa.s/l")
#------------------------------------------------------------

description(select(bdd, X7_mean), 'continuous')

###
#AX
###
AX <- as.data.frame(bdd$AX_mean) %>%
  filter(!is.na(bdd$AX_mean)) %>%
  rename('AX_mean' = 'bdd$AX_mean')

#------------density plot------------------------------------
ggplot(data = AX, aes(AX_mean, y = ..density..)) +
  geom_histogram(binwidth = 5) +
  geom_density(colour = 'purple') + 
  labs(title = "AX mean", x = "")
#------------------------------------------------------------

description(select(bdd, AX_mean), 'continuous')

#######
#R7-19
#######
R7_19 <- as.data.frame(bdd$R7_19_mean) %>%
  filter(!is.na(bdd$R7_19_mean)) %>%
  rename('R7_19_mean' = 'bdd$R7_19_mean')

#------------density plot------------------------------------
ggplot(data = R7_19, aes(R7_19_mean, y = ..density..)) +
  geom_histogram(binwidth = 0.5) +
  geom_density(colour = 'purple') + 
  labs(title = "R7-19 mean", x = "hPa.s/l")
#------------------------------------------------------------

description(select(bdd, R7_19_mean), 'continuous')

#--------------------------Adjustment factors-----------------------------------

###########
#Child sex
###########
ch_sex <- as.data.frame(bdd$ch_sex) %>%
  rename('ch_sex' = 'bdd$ch_sex')

questionr::freq(ch_sex)

##############
#Child height
##############
height_cm <- as.data.frame(bdd$height_cm) %>% 
  rename('height_cm' = 'bdd$height_cm') 

#------------density plot------------------------------------
ggplot(data = height_cm, aes(height_cm, y = ..density..)) +
  geom_histogram(binwidth = 1) +
  geom_density(colour = 'purple') + 
  labs(title = "Child height", x = "cm")
#------------------------------------------------------------

description(select(bdd, height_cm), 'continuous')

##########################
#Tobacco smoking exposure
##########################
#--------3 years
#Yes = 1 / No = 0
#Maternal smoking
#If she smokes during any trim or at t1, t2, t3 -> tobacco smo expo = Yes 
#Else, if she doesn't smoke during any trim or at t1, t2, t3 -> tobacco smo expo = No
tob_factor <- select(bdd, ident, mo_tob_gr_anyt_yn_n2, mo_tob_grt1_yn, mo_tob_grt2_yn,
                     mo_tob_grt3_yn_n2)

tob_expo <- tob_factor %>%
  subset(!is.na(mo_tob_gr_anyt_yn_n2) | !is.na(mo_tob_grt1_yn) |
           !is.na(mo_tob_grt2_yn) | !is.na(mo_tob_grt3_yn_n2)) %>%
  rowwise() %>%
  mutate(tob_expo = max(c_across(mo_tob_gr_anyt_yn_n2:mo_tob_grt3_yn_n2), na.rm = T))

tob_expo <- left_join(tob_factor, tob_expo)

#Passive expo after birth (up to 3 years)
passive_tob_factor <- select(bdd, ident, ch_ETS_12m_opt36m, child_ETS_12m, child_ETS_36m)

passive_tob <- passive_tob_factor %>%
  subset(!is.na(ch_ETS_12m_opt36m) | !is.na(child_ETS_12m) |
           !is.na(child_ETS_36m)) %>%
  rowwise() %>%
  mutate(passive_tob = max(c_across(ch_ETS_12m_opt36m:child_ETS_36m), na.rm = T))

passive_tob <- left_join(passive_tob_factor, passive_tob)

#Total exposure of child
tob_tot_factor <- left_join(passive_tob, tob_expo) %>%
  select(ident, passive_tob, tob_expo)

ch_tob_expo_3y <- tob_tot_factor %>%
  subset(!is.na(tob_expo) | !is.na(passive_tob)) %>%
  rowwise() %>%
  mutate(ch_tob_expo = max(c_across(passive_tob:tob_expo), na.rm = T))

#Description
questionr::freq(ch_tob_expo_3y$ch_tob_expo)

bdd <- left_join(bdd, ch_tob_expo_3y) %>%
  rename(ch_tob_expo_3y = ch_tob_expo)

#--------2 months
#Yes = 1 / No = 0
#Maternal smoking
#If she smokes during any trim or at t1, t2, t3 -> tobacco smo expo = Yes
#Else, if she doesn't smoke during any trim or at t1, t2, t3 -> tobacco smo expo = No
tob_factor <- select(bdd, ident, mo_tob_gr_anyt_yn_n2, mo_tob_grt1_yn, mo_tob_grt2_yn,
                     mo_tob_grt3_yn_n2)

tob_expo <- tob_factor %>%
  subset(!is.na(mo_tob_gr_anyt_yn_n2) | !is.na(mo_tob_grt1_yn) |
           !is.na(mo_tob_grt2_yn) | !is.na(mo_tob_grt3_yn_n2)) %>%
  rowwise() %>%
  mutate(tob_expo = max(c_across(mo_tob_gr_anyt_yn_n2:mo_tob_grt3_yn_n2), na.rm = T))

tob_expo <- left_join(tob_factor, tob_expo)

#Passive expo after birth (up to 2 months)
passive_tob_factor <- select(bdd, ident, Ch2m_ETS_Total_yn)

#Total exposure of child
tob_tot_factor <- left_join(passive_tob_factor, tob_expo) %>%
  select(ident, Ch2m_ETS_Total_yn, tob_expo)

ch_tob_expo_2m <- tob_tot_factor %>%
  subset(!is.na(tob_expo) | !is.na(Ch2m_ETS_Total_yn)) %>%
  rowwise() %>%
  mutate(ch_tob_expo = max(c_across(Ch2m_ETS_Total_yn:tob_expo), na.rm = T))

#Description
questionr::freq(ch_tob_expo_2m$ch_tob_expo)

bdd <- left_join(bdd, ch_tob_expo_2m) %>%
  rename(ch_tob_expo_2m = ch_tob_expo)

####################
#Season of sampling
####################
#0: January-March / 1: April-June / 2: July-September / 3 : October-December 
season <- select(bdd, ident, mo_dte_withdraw_serum_pfas) %>%
  mutate(month = month(as.Date(mo_dte_withdraw_serum_pfas)), #We keep month (only) from dates
         season = case_when(!is.na(month) & month <= 3 ~ '0',
                            !is.na(month) & month <= 6 & month > 3 ~ '1',
                            !is.na(month) & month <= 9 & month > 6 ~ '2',
                            !is.na(month) & month > 9 ~ '3')) %>%
  select(ident, season)

questionr::freq(season$season)

bdd <- left_join(bdd, season)

##################################
#Parents highest educational level
##################################
#parents_dipl = max(mother_dipl, father_dipl)
parents_dipl <- select(bdd, ident, dipl_pere, dipl_mere) %>%
  subset(!is.na(dipl_mere) | !is.na(dipl_pere)) %>%
  rowwise() %>%
  mutate(dipl = max(c_across(dipl_pere:dipl_mere), na.rm = T), #Not equal categories --> We use 2 categories
         dipl = ifelse(dipl <= 2, 1, 2)) %>% #We group the 2 first categories
  select(ident, dipl)

bdd <- left_join(bdd, parents_dipl)

questionr::freq(bdd$dipl)

###############
#Breastfeeding
###############
#More than 2 months = 1 / Less than 2 months = 0
questionr::freq(bdd$ch_still_bf_m2)

###############
#Delivery mode
###############
#1: vaginal mode / 2: caesarean
questionr::freq(bdd$po_delmod)

########
#Parity
########
questionr::freq(bdd$mo_par)

#We transform its categories into 2 categories: 0 -> 0 & 1 -> >0
parity <- select(bdd, ident, mo_par) %>%
  mutate(parity = ifelse(mo_par != 0, 1, 0)) %>%
  mutate_at('parity', as.factor) 

bdd <- left_join(bdd, parity)
questionr::freq(bdd$parity)

################
#Parents asthma
################
#If asthme_mother == 1 or asthme_father == 1 --> ATCD_asthme == 1
#Else if asthme_father == 0 & asthme_mother == 0 --> ATCD_asthme == 0
questionr::freq(bdd$ATCD_asthme)

##################
#Parents rhinite
##################
#If rhinite_mother == 1 or rhinite_father == 1 --> ATCD_rhinite == 1
#Else if rhinite_father == 0 & rhinite_mother == 0 --> ATCD_rhinite == 0
questionr::freq(bdd$ATCD_rhinite)

###############
#Child daycare 
###############
#Dataset from Vicente
ch_daycare <- read_csv("../0_Base_initiale/data_vicente_08july21_mode_of_care_290921.csv",
                                col_names = TRUE)
bdd <- left_join(bdd, ch_daycare, by = 'ident')

#Up to 1 year
questionr::freq(bdd$daycare_up1y_3c)

############################
#Sensibilisation allergique
############################
questionr::freq(bdd$inh_all_sens_SPT_2)

################
#GA at delivery
################
#In days
GA_at_delivery <- select(bdd, ident, po_datedel, po_datelmp) %>%
  mutate(GA_at_delivery = difftime(po_datedel, po_datelmp)) 

bdd <- left_join(bdd, GA_at_delivery)

#------------density plot------------------------------------
ggplot(data = bdd, aes(as.numeric(GA_at_delivery), y = ..density..)) +
  geom_histogram(binwidth = 1) +
  geom_density(colour = 'purple') + 
  labs(title = "GA at delivery", x = "days")
#------------------------------------------------------------

description(select(bdd, GA_at_delivery), 'continuous') #in days

########################################
#Child wheeze or bronchiolitis or asthma
########################################
ch_wh_bron_ast <- select(bdd, ident, ch_wheez_0_3y_opt, ch_bronchiolitis_0_2y_opt,
                       ch_asthma_0_3y_opt) %>%
  subset(!is.na(ch_wheez_0_3y_opt) | !is.na(ch_bronchiolitis_0_2y_opt) |
           !is.na(ch_asthma_0_3y_opt)) %>%
  rowwise() %>%
  mutate(ch_wh_bron_ast = max(c_across(ch_wheez_0_3y_opt:ch_asthma_0_3y_opt), na.rm = T))

bdd <- left_join(bdd, ch_wh_bron_ast)

questionr::freq(ch_wh_bron_ast$ch_wh_bron_ast)

##################
#Fish consumption
##################
#Cat 0 -> low or no consumption
#Cat 1 -> regular or high consumption
questionr::freq(bdd$fish_consumption_T1)

#--------------------------Exposures--------------------------------------------

######
#PFAS
######
PFAS <- select(bdd, ident, mo_pfda_i_cor, mo_PFOS_cor, mo_PFNA,
               mo_PFHxS_cor, mo_PFOA_cor, mo_pfunda_i_cor, mo_pfhps_i_cor)

description(select(PFAS, -ident), 'continuous')

#PFHPS
#------------density plot------------------------------------
ggplot(data = bdd, aes(mo_pfhps_i_cor, y = ..density..)) +
  geom_histogram(binwidth = 0.01) +
  geom_density(colour = 'purple') + 
  labs(title = "PFHPS", x = "")
#------------------------------------------------------------

#PFUnDA
#------------density plot------------------------------------
ggplot(data = bdd, aes(mo_pfunda_i_cor, y = ..density..)) +
  geom_histogram(binwidth = 0.01) +
  geom_density(colour = 'purple') + 
  labs(title = "PFUnDA", x = "")
#------------------------------------------------------------

#PFOA
#------------density plot------------------------------------
ggplot(data = bdd, aes(mo_PFOA_cor, y = ..density..)) +
  geom_histogram(binwidth = 0.1) +
  geom_density(colour = 'purple') + 
  labs(title = "PFOA", x = "")
#------------------------------------------------------------

#PFHxS
#------------density plot------------------------------------
ggplot(data = bdd, aes(mo_PFHxS_cor, y = ..density..)) +
  geom_histogram(binwidth = 0.1) +
  geom_density(colour = 'purple') + 
  labs(title = "PFHxS", x = "")
#------------------------------------------------------------

#PFNA
#------------density plot------------------------------------
ggplot(data = bdd, aes(mo_PFNA, y = ..density..)) +
  geom_histogram(binwidth = 0.1) +
  geom_density(colour = 'purple') + 
  labs(title = "PFNA", x = "")
#------------------------------------------------------------

#PFOS
#------------density plot------------------------------------
ggplot(data = bdd, aes(mo_PFOS_cor, y = ..density..)) +
  geom_histogram(binwidth = 1.0) +
  geom_density(colour = 'purple') + 
  labs(title = "PFOS", x = "")
#------------------------------------------------------------

#PFDA
#------------density plot------------------------------------
ggplot(data = bdd, aes(mo_pfda_i_cor, y = ..density..)) +
  geom_histogram(binwidth = 0.05) +
  geom_density(colour = 'purple') + 
  labs(title = "PFDA", x = "")
#------------------------------------------------------------

##ln-transformation
PFAS <- PFAS %>%
  mutate_if(variable.names(PFAS) != 'ident', log)

description(select(PFAS, -ident), 'continuous')

#PFHPS
#------------density plot------------------------------------
ggplot(data = PFAS, aes(mo_pfhps_i_cor, y = ..density..)) +
  geom_histogram(binwidth = 0.1) +
  geom_density(colour = 'purple') + 
  labs(title = "ln(PFHPS)", x = "")
#------------------------------------------------------------

#PFUnDA
#------------density plot------------------------------------
ggplot(data = PFAS, aes(mo_pfunda_i_cor, y = ..density..)) +
  geom_histogram(binwidth = 0.1) +
  geom_density(colour = 'purple') + 
  labs(title = "ln(PFUnDA)", x = "")
#------------------------------------------------------------

#PFOA
#------------density plot------------------------------------
ggplot(data = PFAS, aes(mo_PFOA_cor, y = ..density..)) +
  geom_histogram(binwidth = 0.1) +
  geom_density(colour = 'purple') + 
  labs(title = "ln(PFOA)", x = "")
#------------------------------------------------------------

#PFHxS
#------------density plot------------------------------------
ggplot(data = PFAS, aes(mo_PFHxS_cor, y = ..density..)) +
  geom_histogram(binwidth = 0.1) +
  geom_density(colour = 'purple') + 
  labs(title = "ln(PFHxS)", x = "")
#------------------------------------------------------------

#PFNA
#------------density plot------------------------------------
ggplot(data = PFAS, aes(mo_PFNA, y = ..density..)) +
  geom_histogram(binwidth = 0.1) +
  geom_density(colour = 'purple') + 
  labs(title = "ln(PFNA)", x = "")
#------------------------------------------------------------

#PFOS
#------------density plot------------------------------------
ggplot(data = PFAS, aes(mo_PFOS_cor, y = ..density..)) +
  geom_histogram(binwidth = 0.3) +
  geom_density(colour = 'purple') + 
  labs(title = "ln(PFOS)", x = "")
#------------------------------------------------------------

#PFDA
#------------density plot------------------------------------
ggplot(data = PFAS, aes(mo_pfda_i_cor, y = ..density..)) +
  geom_histogram(binwidth = 0.1) +
  geom_density(colour = 'purple') + 
  labs(title = "ln(PFDA)", x = "")
#------------------------------------------------------------

#Categorical exposures: 1 -> < LOQ / 2 <- >LOQ
#PFOSA excluded cf document PFAS_Detec_perbatch dans ../3_results/PFAS_cat_%<LOD_%>LOQ

#PFHpA
bdd$mo_PFHpA_cat <- case_when(!is.na(bdd$mo_PFHpA_cat) & bdd$mo_PFHpA_cat == 1 ~ '1',
                              !is.na(bdd$mo_PFHpA_cat) & bdd$mo_PFHpA_cat == 2 ~ '1',
                              !is.na(bdd$mo_PFHpA_cat) & bdd$mo_PFHpA_cat == 3 ~ '2',
                              is.na(bdd$mo_PFHpA_cat) ~ 'NA') %>%
  as.factor()

questionr::freq(bdd$mo_PFHpA_cat)

#PFDoDA
bdd$mo_PFDoDA_cat <- case_when(!is.na(bdd$mo_PFDoDA_cat) & bdd$mo_PFDoDA_cat == 1 ~ '1',
                              !is.na(bdd$mo_PFDoDA_cat) & bdd$mo_PFDoDA_cat == 2 ~ '1',
                              !is.na(bdd$mo_PFDoDA_cat) & bdd$mo_PFDoDA_cat == 3 ~ '2',
                              is.na(bdd$mo_PFDoDA_cat) ~ 'NA') %>%
  as.factor()

questionr::freq(bdd$mo_PFDoDA_cat)

#PFTrDA
bdd$mo_PFTrDA_cat <- case_when(!is.na(bdd$mo_PFTrDA_cat) & bdd$mo_PFTrDA_cat == 1 ~ '1',
                               !is.na(bdd$mo_PFTrDA_cat) & bdd$mo_PFTrDA_cat == 2 ~ '1',
                               !is.na(bdd$mo_PFTrDA_cat) & bdd$mo_PFTrDA_cat == 3 ~ '2',
                               is.na(bdd$mo_PFTrDA_cat) ~ 'NA') %>%
  as.factor()

questionr::freq(bdd$mo_PFTrDA_cat)

#PFBS
bdd$mo_PFBS_cat <- case_when(!is.na(bdd$mo_PFBS_cat) & bdd$mo_PFBS_cat == 1 ~ '1',
                               !is.na(bdd$mo_PFBS_cat) & bdd$mo_PFBS_cat == 2 ~ '1',
                               !is.na(bdd$mo_PFBS_cat) & bdd$mo_PFBS_cat == 3 ~ '2',
                               is.na(bdd$mo_PFBS_cat) ~ 'NA') %>%
  as.factor()

questionr::freq(bdd$mo_PFBS_cat)


#6:2 diPAP
bdd$mo__6_2diPAP_cat <- case_when(!is.na(bdd$mo__6_2diPAP_cat) & bdd$mo__6_2diPAP_cat == 1 ~ '1',
                             !is.na(bdd$mo__6_2diPAP_cat) & bdd$mo__6_2diPAP_cat == 2 ~ '1',
                             !is.na(bdd$mo__6_2diPAP_cat) & bdd$mo__6_2diPAP_cat == 3 ~ '2',
                             is.na(bdd$mo__6_2diPAP_cat) ~ 'NA') %>%
  as.factor()

questionr::freq(bdd$mo__6_2diPAP_cat)

#8:2 diPAP
bdd$mo__8_2diPAP_cat <- case_when(!is.na(bdd$mo__8_2diPAP_cat) & bdd$mo__8_2diPAP_cat == 1 ~ '1',
                                  !is.na(bdd$mo__8_2diPAP_cat) & bdd$mo__8_2diPAP_cat == 2 ~ '1',
                                  !is.na(bdd$mo__8_2diPAP_cat) & bdd$mo__8_2diPAP_cat == 3 ~ '2',
                                  is.na(bdd$mo__8_2diPAP_cat) ~ 'NA') %>%
  as.factor()

questionr::freq(bdd$mo__8_2diPAP_cat)

#PFHxPA
bdd$mo_PFHxPA_cat <- case_when(!is.na(bdd$mo_PFHxPA_cat) & bdd$mo_PFHxPA_cat == 1 ~ '1',
                                  !is.na(bdd$mo_PFHxPA_cat) & bdd$mo_PFHxPA_cat == 2 ~ '1',
                                  !is.na(bdd$mo_PFHxPA_cat) & bdd$mo_PFHxPA_cat == 3 ~ '2',
                                  is.na(bdd$mo_PFHxPA_cat) ~ 'NA') %>%
  as.factor()

questionr::freq(bdd$mo_PFHxPA_cat)

#--------------------------Multiple imputation----------------------------------
bdd <- bdd %>%
  mutate(mo_bmi_bepr = mo_we_bepr / ((mo_he/100)^2)) 

data_imputation <- select(bdd, ident, ch_sex, ch_tob_expo_3y, ch_tob_expo_2m, po_w,
                          taille_2m, poids_2m, dipl, ch_still_bf_m2, season,
                          mo_bmi_bepr, weight, parity, height_cm, daycare_up1y_3c,
                          ATCD_asthme, ATCD_rhinite, mo_PFHpA_cat, mo_PFDoDA_cat,
                          mo_PFBS_cat, mo__6_2diPAP_cat, mo__8_2diPAP_cat,
                          mo_PFHxPA_cat, mo_PFTrDA_cat, fish_consumption_T1,
                          GA_at_delivery, inh_all_sens_SPT_2, ch_wh_bron_ast,
                          mo_he, mo_we_bepr, mo_age) %>%
  left_join(PFAS, by = 'ident') %>%
  mutate_at(vars(ch_still_bf_m2, season, GA_at_delivery), as.numeric) %>%
  mutate_at(vars(ch_still_bf_m2, ch_sex, ch_tob_expo_2m, ch_tob_expo_3y,
                 dipl, season, parity,daycare_up1y_3c, ATCD_rhinite, ATCD_asthme,
                 inh_all_sens_SPT_2, ch_wh_bron_ast, fish_consumption_T1), as.factor) 

set.seed(123)
bdd_imput <- mice(data = data_imputation, m = 20, print = FALSE)
meth <- bdd_imput$method
pred <- bdd_imput$predictorMatrix

#Passive value: BMI / ident ne doit pas prendre part dans l'imputation
meth["mo_bmi_bepr"] <- "~ I(mo_we_bepr / mo_he^2)"
pred[c("mo_we_bepr", "mo_he"), "mo_bmi_bepr"] <- 0
pred[, "ident"] <- 0
pred["ident",] <- 0

#New imputation
set.seed(123)
bdd_imput <- mice(data = data_imputation, method = meth,
                        predictorMatrix = pred, m = 20,
                        print = FALSE)

#Save table in csv format
bdd_imput <- complete(bdd_imput, action = "long", include = TRUE )
write.csv(bdd_imput,file = "../1_Base_intermediaire/bdd_N450PFAS_imputee_221216.csv",row.names = FALSE)
#-------------------------------------------------------------------------------

#Save only data useful for our analyses
bdd <- select(bdd, ident, X7_mean, R7_mean,
              AX_mean, R7_19_mean, ch2m_LCI2_corrHV,
              ch2m_CRF2_corrHV, tidalvolumeml, ratiopefexptime, ch_asthma_0_3y_opt,
              ch_wheez_0_3y_opt, ch_bronchitis_0_3y_opt, ch_bronchiolitis_0_2y_opt)

bdd <- left_join(bdd_imput, bdd, by = 'ident')

write.csv(bdd, '../1_Base_intermediaire/bdd_N450PFAS_imputee_nettoyee_221216.csv', row.names = FALSE)
